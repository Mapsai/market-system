<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class AccessToken.
 *
 * @param integer $id ID.
 * @param string $access_token Access Token.
 * @param integer $valid_until Valid until (timestamp in seconds).
 * @param integer $user_id User ID.
 */
class AccessToken extends Model
{
    const TOKEN_LENGTH = 40;
    const TOKEN_VALIDITY_DURATION = 86400; // In seconds. 86400 - 1 day

    public $timestamps = false;
    protected $fillable = ['access_token', 'valid_until', 'user_id'];

    /**
     * Relationship with User.
     *
     * @return HasOne User data.
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}