<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductCategory.
 *
 * @param integer $id ID.
 * @param string $name Name.
 * @param integer $user_id User ID.
 */
class ProductCategory extends Model
{
    protected $fillable = ['user_id', 'name'];
}