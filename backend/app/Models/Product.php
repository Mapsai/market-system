<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Product.
 *
 * @param integer $id ID.
 * @param integer $category_id Category ID.
 * @param integer $user_id User ID.
 * @param string $name Name.
 * @param string $description Description.
 * @param double $price Price.
 */
class Product extends Model
{
    /**
     * @inheritdoc
     */
    protected $fillable = [
        'category_id',
        'name',
        'description',
        'price',
    ];

    /**
     * Relationship with ProductCategory.
     *
     * @return HasOne ProductCategory data.
     */
    public function category()
    {
        return $this->hasOne(ProductCategory::class, 'id', 'category_id');
    }

    /**
     * Relationship with User.
     *
     * @return HasOne User data.
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}