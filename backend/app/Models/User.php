<?php

namespace App\models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User.
 *
 * @param integer $id ID.
 * @param string $name Name.
 * @param string $email Email.
 * @param string $password Hashed password.
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * Relationship with ProductCategory.
     *
     * @return HasMany ProductCategory data.
     */
    public function productCategory()
    {
        return $this->hasMany(ProductCategory::class, 'user_id', 'id');
    }

    /**
     * Relationship with Product.
     *
     * @return HasMany Product data.
     */
    public function product()
    {
        return $this->hasMany(Product::class, 'user_id', 'id');
    }
}