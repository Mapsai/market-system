<?php

namespace App\Http\Controllers;

use App\models\AccessToken;
use App\models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class SiteController.
 */
class SiteController extends Controller
{
    /**
     * Attemps to log in User.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function login(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['email', 'password']);
        $userExists = User::where(['email' => $body['email']])->first();

        if (is_null($userExists) || !Hash::check($body['password'], $userExists->password)) {
            return response()->json([
                'success' => false,
                'message' => 'User not found or incorrect email/password.',
            ]);
        }

        $token = new AccessToken([
            'access_token' => str_random(AccessToken::TOKEN_LENGTH),
            'valid_until' => time() + AccessToken::TOKEN_VALIDITY_DURATION,
            'user_id' => $userExists->id,
        ]);
        $token->save();

        return response()->json([
            'success' => true,
            'message' => '',
            'token' => $token->access_token,
        ]);
    }

    /**
     * Registers a new User.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function register(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['name', 'email', 'password']);
        $validator = $this->validateAttributes($request);

        if ($validator->fails()) {
            $attribute = key($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => 'Registration failed because: ' . $validator->getMessageBag()->getMessages()[$attribute][0],
            ]);
        }

        $user = new User([
            'name' => $body['name'],
            'email' => $body['email'],
            'password' => Hash::make($body['password']),
        ]);

        if ($user->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Registration was successful. You may now log in.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Unknown error during registration. Please try again.',
        ]);
    }

    /**
     * Returns a list of Users.
     *
     * @return JsonResponse Action response.
     */
    public function getUsers()
    {
        return response()->json([
            'success' => true,
            'message' => '',
            'users' => User::all(['id', 'name', 'email', 'created_at']),
        ]);
    }

    /**
     * Returns User's data.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function getUser(Request $request)
    {
        $user = User::with('productCategory')->with('product')->where(['id' => $request->get('id')])->first();

        if (is_null($user)) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.',
            ]);
        }

        $formattedResponse = $this->formatUserCreatedObjects($user);

        return response()->json([
            'success' => true,
            'message' => '',
            'content' => $formattedResponse,
        ]);
    }

    /**
     * Returns owner's data.
     *
     * @return JsonResponse Action response.
     */
    public function getOwner()
    {
        $user = User::where(['id' => $this->user->id])->first();

        return response()->json([
            'success' => true,
            'message' => '',
            'name' => $user->name,
        ]);
    }

    /**
     * Validates user attributes (when creating, updating, etc.).
     *
     * @param Request $request Request object.
     * @return \Illuminate\Contracts\Validation\Validator Validator object.
     */
    private function validateAttributes(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|max:255|email',
            'password' => 'required|string|max:255',
        ]);
    }

    /**
     * Formats a list of User's data, created Product Categories and Products.
     *
     * @param User $user
     * @return array Formatted list.
     */
    private function formatUserCreatedObjects($user)
    {
        $formattedResponse['user'] = [
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => $user->created_at->toDatetimeString(),
        ];

        foreach ($user->productCategory as $productCategory) {
            $formattedResponse['categories'][] = [
                'id' => $productCategory->id,
                'name' => $productCategory->name,
                'created_at' => $productCategory->created_at->toDatetimeString(),
            ];
        }

        foreach ($user->product as $product) {
            $formattedResponse['products'][] = [
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description,
                'price' => $product->price,
                'created_at' => $product->created_at->toDatetimeString(),
            ];
        }

        return $formattedResponse;
    }
}