<?php

namespace App\Http\Controllers;

use App\models\AccessToken;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class Controller.
 *
 * Contains global methods that can be used by any child controller.
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $user = null;

    /**
     * @inheritdoc
     */
    public function callAction($method, $parameters)
    {
        if (!empty($parameters[0])) {
            $token = $parameters[0]->header('Access-Token');
        } elseif (!empty(getallheaders()['Access-Token'])) {
            $token = getallheaders()['Access-Token'];
        } else {
            $token = null;
        }

        $methodsForGuests = ['login', 'register']; // A list of actions allowed for guests only
        $accessToken = AccessToken::with('user')->where(['access_token' => $token])->first();

        if (is_null($accessToken) || $accessToken->valid_until < time()) {
            if (in_array($method, $methodsForGuests)) {
                return parent::callAction($method, $parameters);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Forbidden. Access token is missing or expired.',
                    'token' => false,
                ]);
            }
        }

        // A user trying to access guest-only actions
        if (in_array($method, $methodsForGuests)) {
            return response()->json([
                'success' => false,
                'message' => 'Forbidden. Only guests can access this page.',
                'token' => false,
            ]);
        }

        $this->user = $accessToken->user;
        return parent::callAction($method, $parameters);
    }

    /**
     * Checks whether all required indexes present in a given array.
     *
     * @param array $arrayToCheck Array to check (User input).
     * @param array $requiredIndexes A list of indexes that are required to be present.
     * @return null|JsonResponse Action response.
     * @see https://www.reddit.com/r/PHP/comments/2jitxn/best_way_to_check_an_array_for_existence_of/clc5i3y
     */
    public function containsAllIndexes($arrayToCheck, $requiredIndexes)
    {
        if (
            is_null($arrayToCheck) ||
            count(array_intersect(array_keys($arrayToCheck), $requiredIndexes)) != count($requiredIndexes)
        ) {
            exit(response()->json([
                'success' => false,
                'message' => 'Insufficient parameters.',
            ]));
        }

        return null;
    }
}