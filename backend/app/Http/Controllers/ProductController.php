<?php

namespace App\Http\Controllers;

use App\models\Product;
use App\models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProductController.
 */
class ProductController extends Controller
{
    /**
     * Returns a list of Products.
     *
     * @return JsonResponse Action response.
     */
    public function getProducts()
    {
        $products = Product::with('category')->with('user')->get();
        $formattedProducts = [];

        foreach ($products as $i => $product) {
            $formattedProducts[] = [
                'id' => $product->id,
                'name' => $product->name,
                'user' => $product->user->name,
                'category' => $product->category->name,
                'description' => $product->description,
                'price' => $product->price,
            ];
        }

        return response()->json([
            'success' => true,
            'message' => '',
            'products' => $formattedProducts,
        ]);
    }

    /**
     * Gets Product's data.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function getProduct(Request $request)
    {
        $product = Product::where(['id' => $request->get('id')])
            ->select('category_id', 'name', 'description', 'price')
            ->first();

        if (is_null($product)) {
            return response()->json([
                'success' => false,
                'message' => 'Product not found.',
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => '',
            'product' => $product,
        ]);
    }

    /**
     * Creates a new Product.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function createProduct(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['category', 'name', 'description', 'price']);
        $productCategory = ProductCategory::where(['id' => $body['category']])->first();

        if (is_null($productCategory)) {
            return response()->json([
                'success' => false,
                'message' => 'Product category not found.',
            ]);
        }

        $validator = $this->validateAttributes($request);

        if ($validator->fails()) {
            $attribute = key($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => 'Saving failed because: ' . $validator->getMessageBag()->getMessages()[$attribute][0],
            ]);
        }

        $product = new Product([
            'category_id' => $productCategory->id,
            'name' => $body['name'],
            'description' => $body['description'],
            'price' => $body['price'],
        ]);
        $product->user_id = $this->user->id; // Not fillable

        if ($product->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Product was successfully created.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Failed to create a product. Please try again.',
        ]);
    }

    /**
     * Updates existing Product.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function updateProduct(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['id', 'category_id', 'name', 'description', 'price']);
        $product = Product::where(['id' => $body['id']])->first();
        $productCategory = ProductCategory::where(['id' => $body['category_id']])->first();

        if (is_null($product) || is_null($productCategory)) {
            return response()->json([
                'success' => false,
                'message' => 'Product or product category not found.',
            ]);
        }

        $validator = $this->validateAttributes($request);

        if ($validator->fails()) {
            $attribute = key($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => 'Update failed because: ' . $validator->getMessageBag()->getMessages()[$attribute][0],
            ]);
        }

        $product->fill($request->all());

        if ($product->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Product was successfully updated.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Failed to update a product. Please try again.',
        ]);
    }

    /**
     * Deletes existing Product.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function deleteProduct(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['id']);
        $product = Product::where(['id' => $body['id']])->first();

        if (is_null($product)) {
            return response()->json([
                'success' => false,
                'message' => 'Product not found.',
            ]);
        }

        if ($product->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Product was successfully deleted.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Failed to delete a product. Please try again.',
        ]);
    }

    /**
     * Validates product attributes (when creating, updating, etc.).
     *
     * @param Request $request Request object.
     * @return \Illuminate\Contracts\Validation\Validator Validator object.
     */
    private function validateAttributes(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'price' => 'required|numeric',
        ]);
    }
}