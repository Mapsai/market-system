<?php

namespace App\Http\Controllers;

use App\models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProductCategoryController.
 */
class ProductCategoryController extends Controller
{
    /**
     * Returns a list of Product Categories.
     *
     * @return ProductCategory[] A list of Product Categories.
     */
    public function categories()
    {
        return response()->json([
            'success' => true,
            'message' => '',
            'categories' => ProductCategory::all(['id', 'name']),
        ]);
    }

    /**
     * Returns a list of Product Categories.
     *
     * @return JsonResponse Action response.
     */
    public function getCategories()
    {
        return response()->json([
            'success' => true,
            'message' => '',
            'categories' => ProductCategory::all(['id', 'name']),
        ]);
    }

    /**
     * Gets Product category's data.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function getCategory(Request $request)
    {
        $productCategory = ProductCategory::where(['id' => $request->get('id')])->select('id', 'name')->first();

        if (is_null($productCategory)) {
            return response()->json([
                'success' => false,
                'message' => 'Product category not found.',
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => '',
            'category' => $productCategory,
        ]);
    }

    /**
     * Creates a new Product Category.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function createCategory(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['name']);
        $validator = $this->validateAttributes($request);

        if ($validator->fails()) {
            $attribute = key($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => 'Saving failed because: ' . $validator->getMessageBag()->getMessages()[$attribute][0],
            ]);
        }

        $productCategory = new ProductCategory([
            'name' => $body['name'],
            'user_id' => $this->user->id,
        ]);

        if ($productCategory->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Product category was successfully created.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Failed to create a product category. Please try again.',
        ]);
    }

    /**
     * Updates existing Product Category.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function updateCategory(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['id', 'name']);
        $productCategory = ProductCategory::where(['id' => $body['id']])->first();

        if (is_null($productCategory)) {
            return response()->json([
                'success' => false,
                'message' => 'Product category not found.',
            ]);
        }

        $validator = $this->validateAttributes($request);

        if ($validator->fails()) {
            $attribute = key($validator->getMessageBag()->getMessages());
            return response()->json([
                'success' => false,
                'message' => 'Update failed because: ' . $validator->getMessageBag()->getMessages()[$attribute][0],
            ]);
        }

        $productCategory->name = $body['name'];

        if ($productCategory->save()) {
            return response()->json([
                'success' => true,
                'message' => 'Product category was successfully updated.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Failed to update a product category. Please try again.',
        ]);
    }

    /**
     * Deletes existing Product Category.
     *
     * @param Request $request
     * @return JsonResponse Action response.
     */
    public function deleteCategory(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        $this->containsAllIndexes($body, ['id']);
        $category = ProductCategory::where(['id' => $body['id']])->first();

        if (is_null($category)) {
            return response()->json([
                'success' => false,
                'message' => 'Product category not found.',
            ]);
        }

        if ($category->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Category was successfully deleted.',
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Failed to delete a category. Please try again.',
        ]);
    }

    /**
     * Validates product category attributes (when creating, updating, etc.).
     *
     * @param Request $request Request object.
     * @return \Illuminate\Contracts\Validation\Validator Validator object.
     */
    private function validateAttributes(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);
    }
}