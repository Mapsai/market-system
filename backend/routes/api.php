<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'SiteController@login');
Route::post('register', 'SiteController@register');
Route::get('get-users', 'SiteController@getUsers');
Route::get('get-user', 'SiteController@getUser');
Route::get('get-owner', 'SiteController@getOwner');
Route::get('get-products', 'ProductController@getProducts');
Route::get('get-product', 'ProductController@getProduct');
Route::post('create-product', 'ProductController@createProduct');
Route::post('update-product', 'ProductController@updateProduct');
Route::post('delete-product', 'ProductController@deleteProduct');
Route::get('categories', 'ProductCategoryController@categories');
Route::get('get-categories', 'ProductCategoryController@getCategories');
Route::get('get-category', 'ProductCategoryController@getCategory');
Route::post('create-category', 'ProductCategoryController@createCategory');
Route::post('update-category', 'ProductCategoryController@updateCategory');
Route::post('delete-category', 'ProductCategoryController@deleteCategory');