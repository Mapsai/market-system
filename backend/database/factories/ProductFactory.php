<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => rand(1, ProductCategorySeeder::DUMMY_DATA_AMOUNT),
        'user_id' => rand(1, UserSeeder::DUMMY_DATA_AMOUNT),
        'name' => $faker->words(rand(1, 4), true),
        'description' => $faker->paragraph,
        'price' => $faker->randomFloat(2, 0, 100),
        'image_name' => $faker->words(rand(1, 4), true) . $faker->fileExtension,
    ];
});