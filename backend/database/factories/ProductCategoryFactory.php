<?php

use App\ProductCategory;
use Faker\Generator as Faker;

$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->words(rand(1, 4), true),
        'user_id' => rand(1, UserSeeder::DUMMY_DATA_AMOUNT),
    ];
});