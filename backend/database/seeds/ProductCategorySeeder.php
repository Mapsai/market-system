<?php

use App\models\ProductCategory;
use Faker\Factory;
use Illuminate\Database\Seeder;

/**
 * Class ProductCategorySeeder.
 */
class ProductCategorySeeder extends Seeder
{
    const DUMMY_DATA_AMOUNT = 20;

    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < self::DUMMY_DATA_AMOUNT; $i++) {
            ProductCategory::create([
                'name' => $faker->words(rand(1, 4), true),
                'user_id' => rand(1, UserSeeder::DUMMY_DATA_AMOUNT),
            ]);
        }
    }
}