<?php

use App\models\Product;
use Faker\Factory;
use Illuminate\Database\Seeder;

/**
 * Class ProductSeeder.
 */
class ProductSeeder extends Seeder
{
    const DUMMY_DATA_AMOUNT = 20;

    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < self::DUMMY_DATA_AMOUNT; $i++) {
            Product::create([
                'category_id' => rand(1, ProductCategorySeeder::DUMMY_DATA_AMOUNT),
                'user_id' => rand(1, UserSeeder::DUMMY_DATA_AMOUNT),
                'name' => $faker->words(rand(1, 4), true),
                'description' => $faker->sentences(3, true),
                'price' => $faker->randomFloat(2, 0, 100),
            ]);
        }
    }
}