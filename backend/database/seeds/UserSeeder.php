<?php

use App\models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserSeeder.
 */
class UserSeeder extends Seeder
{
    const DUMMY_DATA_AMOUNT = 20;

    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < self::DUMMY_DATA_AMOUNT; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('password'),
            ]);
        }
    }
}