# Market System #

This is a Market System web application, build with Vue.js + Laravel frameworks.

# Installation #

**If using "serve" for Vue.js and Laravel:**

Vue.js:

- In case Laravel's URL port is not 8000, edit frontend/src/helpers/axios.js file and replace port number to existing one (on line 13);
- Run "npm run build";

Laravel:

- Edit .env file to setup a database connection;
- Run "php artisan migrate";
- If needed, also run "php artisan db:seed" to generate some dummy data;
- Run "php artisan serve";


**Otherwise (for example, uploading to live server):**

Vue.js:

- Run "npm run build";
- Move dist/ to root;
- Add frontend/.htaccess file to root;

Laravel:

- Edit .env file to setup a database connection;
- Move public/index.php to root;
- Create laravel/ (or something similar) folder above root;
- Move everything from laravel development folder to newly created laravel/ folder;
- Edit index.php file to match file paths;
- Run "php artisan migrate";
- If needed, also run "php artisan db:seed" to generate some dummy data;