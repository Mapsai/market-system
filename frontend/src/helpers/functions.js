import Vue from 'vue';

export const getToken = () => {
    return Vue.localStorage.get('access-token');
};

export const setToken = (token) => {
    return Vue.localStorage.set('access-token', token);
};

export const removeToken = () => {
    Vue.localStorage.remove('access-token');
};