import axios from 'axios';
import store from '../store/store';
import { router } from '../routing/router';
import { removeToken } from './functions';

axios.interceptors.request.use((config) => {
    /**
     *  Since port is different (if used "serve"), we can't use window.location.origin for both live and localhost servers.
     *  Change 8000 port to different if Laravel's default port is not 8000.
    */
    config.baseURL = (
        location.host.indexOf('localhost:') !== -1 ?
            'http://localhost:8000/api/' :
            window.location.origin + '/index.php/api/'
    );
    config.headers = { 'Access-Token': store.getters.accessToken };
    return config;
});

axios.interceptors.response.use((response) => {
    let data = response.data;

    if (data.success === false && data.message !== '' && data.token === false) {
        store.dispatch('setAlert', { message: data.message, type: 'error' });
        store.dispatch('setAccessToken', null);
        removeToken();
        router.push({ name: 'Login' });
        return response;
    }

    if (data.success === false && data.message !== '') {
        store.dispatch('setAlert', { message: data.message, type: 'error' });
        return response;
    }

    if (data.success === true && data.message !== '') {
        store.dispatch('setAlert', { message: data.message, type: 'success' });
        return response;
    }

    return response;
}, function(error) {
    store.dispatch('setAlert', { message: 'Request failed (#' + error.response.status +'). Please try refreshing the page.', type: 'error' });
    return Promise.reject(error.response);
});