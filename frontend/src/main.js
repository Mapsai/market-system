import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import axios from 'axios';
import VueAxios from 'vue-axios';
import localStorage from 'vue-localstorage';
import VeeValidate from 'vee-validate';
import './helpers/axios';
import { router } from './routing/router';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueAxios, axios);
Vue.use(localStorage);
Vue.use(VeeValidate);

new Vue({
    render: h => h(App),
    router: router,
    store: require('./store/store'),
}).$mount('#app');