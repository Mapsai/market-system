import Vuex from 'vuex';
import alert from './modules/alert';
import tokens from './modules/token';

export default new Vuex.Store({
    modules: {
        alert,
        tokens,
    },
});