import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    state: {
        accessToken: null,
    },
    getters: {
        accessToken: state => {
            return state.accessToken;
        },
    },
    mutations: {
        SET_ACCESS_TOKEN: (state, payload) => {
            state.accessToken = payload;
        },
    },
    actions: {
        setAccessToken: ({commit}, payload) => {
            commit('SET_ACCESS_TOKEN', payload);
        },
    },
}