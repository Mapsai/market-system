import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default {
    state: {
        visible: false,
        message: '',
        type: 'error',
    },
    getters: {
        alert: state => {
            return {
                visible: state.visible,
                message: state.message,
                type: state.type,
            };
        },
    },
    mutations: {
        SET_ALERT: (state, payload) => {
            state.visible = (payload.visible !== undefined ? payload.visible : true);
            state.message = (payload.message ? payload.message : '');

            if (payload.type) {
                state.type = payload.type;
            }
        },
    },
    actions: {
        setAlert: ({commit}, payload) => {
            commit('SET_ALERT', payload);

            setTimeout(() => {
                commit('SET_ALERT', { visible: false });
            }, 2500);
        },
    },
};