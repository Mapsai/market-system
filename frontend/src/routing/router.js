import VueRouter from 'vue-router';
import store from '../store/store';
import { routes } from './routes';
import { getToken, removeToken } from '../helpers/functions';

export const router = new VueRouter({
    routes: routes,
    mode: 'history',
});

router.beforeEach((to, from, next) => {
    let accessToken = getToken();
    let accessibleToGuests = ['Login', 'Register'];

    if (to.name === 'Logout') {
        removeToken();
        store.dispatch('setAccessToken', null);
        router.push({ name: 'Login' });
        return true;
    }

    // A guest trying to access user-only pages...
    if (accessToken === null && accessibleToGuests.indexOf(to.name) === -1) {
        router.push({ name: 'Login' });
        return true;
    }

    // A user trying to access guest-only pages..
    if (accessToken !== null && accessibleToGuests.indexOf(to.name) !== -1) {
        router.push({ name: 'Home' });
        return true;
    }

    next();
});