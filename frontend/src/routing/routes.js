import Home from '../components/global/Home';
import Login from '../components/user/Login';
import Register from '../components/user/Register';
import Users from '../components/user/Users';
import ViewUser from '../components/user/ViewUser';
import ProductCategories from '../components/category/ProductCategories';
import CreateProductCategory from '../components/category/CreateProductCategory';
import UpdateProductCategory from '../components/category/UpdateProductCategory';
import Products from '../components/product/Products';
import CreateProduct from '../components/product/CreateProduct';
import UpdateProduct from '../components/product/UpdateProduct';
import NotFound from '../components/global/NotFound';

export const routes = [
    {
        path: '',
        component: Home,
        name: 'Home',
    },
    {
        path: '/login',
        component: Login,
        name: 'Login',
    },
    {
        path: '/register',
        component: Register,
        name: 'Register',
    },
    {
        path: '/users',
        component: Users,
        name: 'Users',
    },
    {
        path: '/view-user/:id',
        component: ViewUser,
        name: 'ViewUser',
    },
    {
        path: '/product-categories',
        component: ProductCategories,
        name: 'ProductCategories',
    },
    {
        path: '/create-categories',
        component: CreateProductCategory,
        name: 'CreateProductCategory',
    },
    {
        path: '/update-category/:id',
        component: UpdateProductCategory,
        name: 'UpdateProductCategory',
    },
    {
        path: '/products',
        component: Products,
        name: 'Products',
    },
    {
        path: '/create-product',
        component: CreateProduct,
        name: 'CreateProduct',
    },
    {
        path: '/update-product/:id',
        component: UpdateProduct,
        name: 'UpdateProduct',
    },
    {
        path: '/logout',
        name: 'Logout',
    },
    {
        path: '*',
        component: NotFound,
        name: 'NotFound',
    },
];